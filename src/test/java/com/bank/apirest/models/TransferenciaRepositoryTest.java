package com.bank.apirest.models;

import com.bank.apirest.repository.ContaRepository;
import com.bank.apirest.repository.TransferenciaRepository;
import org.assertj.core.api.Assertions;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class TransferenciaRepositoryTest {

    @Autowired
    private TransferenciaRepository transferenciaRepository;

    @Autowired
    private ContaRepository contaRepository;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void efetuarTransferencia(){

        Optional<Conta> conta = contaRepository.findContaByNumeroConta("60694-4");
        Transferencia transferencia = new Transferencia("0124-4", "60694-4", "0124-4", "6722-4", 500.00, conta.get());
        this.transferenciaRepository.save(transferencia);

        Assertions.assertThat(transferencia.getId()).isNotNull();
        Assertions.assertThat(transferencia.getNumeroAgenciaOrigem()).isEqualTo("0124-4");
        Assertions.assertThat(transferencia.getNumeroContaOrigem()).isEqualTo("60694-4");
        Assertions.assertThat(transferencia.getNumeroAgenciaDestino()).isEqualTo("0124-4");
        Assertions.assertThat(transferencia.getNumeroContaDestino()).isEqualTo("6722-4");
        Assertions.assertThat(transferencia.getValorTransferencia()).isEqualTo(500.00);
        Assertions.assertThat(transferencia.getConta()).isEqualTo(conta.get());

    }

}
