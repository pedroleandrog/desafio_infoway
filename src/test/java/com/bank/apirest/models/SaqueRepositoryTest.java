package com.bank.apirest.models;

import com.bank.apirest.repository.ContaRepository;
import com.bank.apirest.repository.DepositoRepository;
import com.bank.apirest.repository.SaqueRepository;
import org.assertj.core.api.Assertions;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class SaqueRepositoryTest {

    @Autowired
    private SaqueRepository saqueRepository;

    @Autowired
    private ContaRepository contaRepository;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void efetuaSaque(){
        Optional<Conta> conta = contaRepository.findContaByNumeroConta("60694-4");
        Saque saque = new Saque("0124-4", "60694-4", 300.00, conta.get());
        this.saqueRepository.save(saque);

        Assertions.assertThat(saque.getId()).isNotNull();
        Assertions.assertThat(saque.getNumeroAgencia()).isEqualTo("0124-4");
        Assertions.assertThat(saque.getNumeroConta()).isEqualTo("60694-4");
        Assertions.assertThat(saque.getValorSaque()).isEqualTo(300.00);
        Assertions.assertThat(saque.getConta()).isEqualTo(conta.get());
    }
}
