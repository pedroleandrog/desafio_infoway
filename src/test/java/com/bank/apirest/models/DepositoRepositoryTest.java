package com.bank.apirest.models;

import com.bank.apirest.repository.ContaRepository;
import com.bank.apirest.repository.DepositoRepository;
import org.assertj.core.api.Assertions;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class DepositoRepositoryTest {

    @Autowired
    private DepositoRepository depositoRepository;

    @Autowired
    private ContaRepository contaRepository;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Test
    public void efetuarDeposito(){

        Optional<Conta> conta = contaRepository.findContaByNumeroConta("60694-4");
        Deposito deposito = new Deposito("0124-4", "60694-4", 500.00, conta.get());
        this.depositoRepository.save(deposito);

        Assertions.assertThat(deposito.getId()).isNotNull();
        Assertions.assertThat(deposito.getNumeroAgencia()).isEqualTo("0124-4");
        Assertions.assertThat(deposito.getNumeroConta()).isEqualTo("60694-4");
        Assertions.assertThat(deposito.getValorDeposito()).isEqualTo(500.00);
        Assertions.assertThat(deposito.getConta()).isEqualTo(conta.get());

    }

}
