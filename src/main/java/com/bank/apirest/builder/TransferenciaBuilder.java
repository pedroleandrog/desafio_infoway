package com.bank.apirest.builder;

public class TransferenciaBuilder {

    private String numeroAgenciaOrigem;

    private String numeroContaOrigem;

    private String numeroAgenciaDestino;

    private String numeroContaDestino;

    private double valorTransferencia;

    public String getNumeroAgenciaOrigem() {
        return numeroAgenciaOrigem;
    }

    public void setNumeroAgenciaOrigem(String numeroAgenciaOrigem) {
        this.numeroAgenciaOrigem = numeroAgenciaOrigem;
    }

    public String getNumeroContaOrigem() {
        return numeroContaOrigem;
    }

    public void setNumeroContaOrigem(String numeroContaOrigem) {
        this.numeroContaOrigem = numeroContaOrigem;
    }

    public String getNumeroAgenciaDestino() {
        return numeroAgenciaDestino;
    }

    public void setNumeroAgenciaDestino(String numeroAgenciaDestino) {
        this.numeroAgenciaDestino = numeroAgenciaDestino;
    }

    public String getNumeroContaDestino() {
        return numeroContaDestino;
    }

    public void setNumeroContaDestino(String numeroContaDestino) {
        this.numeroContaDestino = numeroContaDestino;
    }

    public double getValorTransferencia() {
        return valorTransferencia;
    }

    public void setValorTransferencia(double valorTransferencia) {
        this.valorTransferencia = valorTransferencia;
    }
}
