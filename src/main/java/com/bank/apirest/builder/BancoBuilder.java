package com.bank.apirest.builder;

public class BancoBuilder {

    private String nome;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
}
