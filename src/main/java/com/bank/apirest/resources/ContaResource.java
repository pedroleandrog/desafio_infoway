package com.bank.apirest.resources;

import com.bank.apirest.builder.ContaBuilder;
import com.bank.apirest.models.*;
import com.bank.apirest.repository.*;
import com.bank.apirest.rest.DefaultResponse;
import com.bank.apirest.service.ContaService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

@RestController
@RequestMapping(value = "/api")
@Api(value = "API Rest Conta")
@CrossOrigin(origins = "*")
public class ContaResource {

    @Autowired
    ContaRepository contaRepository;

    @Autowired
    AgenciaRepository agenciaRepository;

    @Autowired
    ClienteRepository clienteRepository;

    @Autowired
    SaqueRepository saqueRepository;

    @Autowired
    DepositoRepository depositoRepository;

    @Autowired
    TransferenciaRepository transferenciaRepository;

    @Autowired
    ContaService contaService;

    @GetMapping("/conta")
    @ApiOperation(value = "Retorna uma lista de contas cadastradas")
    public List<Conta> listarContas() {
        List<Conta> contas = new ArrayList<Conta>();
        contaRepository.findAll()
                .forEach(new Consumer<Conta>() {
                    @Override
                    public void accept(Conta conta) {
                        List<Saque> saqueList = saqueRepository.findSaqueByConta(conta);
                        List<Deposito> depositoList = depositoRepository.findDepositoByConta(conta);
                        List<Transferencia> transferenciaList = transferenciaRepository.findTransferenciaByConta(conta);
                        conta.setSaques(saqueList);
                        conta.setDepositos(depositoList);
                        conta.setTransferencias(transferenciaList);
                        contas.add(conta);
                    }
                });
        return contas;
    }

    @GetMapping("/conta/{id}")
    @ApiOperation(value = "Retorna uma conta unica")
    public Conta listarContaUnica(@PathVariable(value = "id") long id) {
        return contaRepository.findById(id);
    }

    @PostMapping("/conta")
    @ApiOperation("Cadastra uma nova conta bancaria em uma agencia ja cadastrada")
    public DefaultResponse cadastrarConta(@RequestBody ContaBuilder contaBuilder) {
        return contaService.save(contaBuilder);
    }

    @DeleteMapping("/conta")
    @ApiOperation(value = "Deleta uma conta")
    public DefaultResponse deletarConta(@RequestParam long id) {
        return contaService.deleteById(id);
    }

    @PutMapping("/conta")
    @ApiOperation("Atualiza as informaçoes da conta bancaria")
    public DefaultResponse atualizarConta(@RequestBody Conta conta) {
        return contaService.update(conta);
    }
}
