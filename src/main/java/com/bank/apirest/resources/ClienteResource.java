package com.bank.apirest.resources;

import com.bank.apirest.builder.ClienteBuilder;
import com.bank.apirest.models.Cliente;
import com.bank.apirest.models.Conta;
import com.bank.apirest.repository.ClienteRepository;
import com.bank.apirest.repository.ContaRepository;
import com.bank.apirest.rest.DefaultResponse;
import com.bank.apirest.service.ClienteService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

@RestController
@RequestMapping(value = "api")
@Api(value = "API Rest Cliente")
public class ClienteResource {

    @Autowired
    ContaRepository contaRepository;

    @Autowired
    ClienteRepository clienteRepository;

    @Autowired
    ClienteService clienteService;

    @GetMapping("/cliente")
    @ApiOperation(value = "Retorna uma lista de Clientes cadastrados")
    public List<Cliente> listarClientes() {
        List<Cliente> clientes = new ArrayList<Cliente>();

        clienteRepository.findAll()
                .forEach(new Consumer<Cliente>() {
                    @Override
                    public void accept(Cliente cliente) {
                        List<Conta> contaList = contaRepository.findContasByCliente(cliente);
                        cliente.setContas(contaList);
                        clientes.add(cliente);
                    }
                });

        return clientes;
    }

    @GetMapping("/cliente/{id}")
    @ApiOperation(value = "Retorna uma conta unica")
    public Cliente listarClienteUnico(@PathVariable(value = "id") long id) {
        return clienteRepository.findById(id);
    }

    @PostMapping("/cliente")
    @ApiOperation("Cadastra um novo cliente")
    public DefaultResponse cadastrarCliente(@RequestBody ClienteBuilder clienteBuilder) {
        return clienteService.save(clienteBuilder);
    }

    @DeleteMapping("/cliente")
    @ApiOperation(value = "Deleta um cliente")
    public DefaultResponse deletarCliente(@RequestParam long id) {
         return clienteService.deleteById(id);
    }

    @PutMapping("/cliente")
    @ApiOperation(value = "Atualiza as informaçoes do cliente")
    public DefaultResponse atualizarCliente(@RequestParam long idCliente, @RequestBody ClienteBuilder clienteBuilder) {
        return clienteService.update(idCliente, clienteBuilder);
    }
}
