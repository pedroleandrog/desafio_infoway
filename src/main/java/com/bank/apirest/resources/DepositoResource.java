package com.bank.apirest.resources;

import com.bank.apirest.builder.DepositoBuilder;
import com.bank.apirest.builder.SaqueBuilder;
import com.bank.apirest.repository.AgenciaRepository;
import com.bank.apirest.repository.ClienteRepository;
import com.bank.apirest.repository.ContaRepository;
import com.bank.apirest.repository.SaqueRepository;
import com.bank.apirest.rest.DefaultResponse;
import com.bank.apirest.service.ContaService;
import com.bank.apirest.service.DepositoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/api")
@Api(value = "API Rest Deposito")
@CrossOrigin(origins = "*")
public class DepositoResource {

    @Autowired
    ContaRepository contaRepository;

    @Autowired
    AgenciaRepository agenciaRepository;

    @Autowired
    ClienteRepository clienteRepository;

    @Autowired
    SaqueRepository saqueRepository;

    @Autowired
    ContaService contaService;

    @Autowired
    DepositoService depositoService;

    @PostMapping("/deposito")
    @ApiOperation("Efetua uma operaçao de deposito na conta informada")
    public DefaultResponse efetuarSaque(@RequestBody DepositoBuilder depositoBuilder) {
        return depositoService.save(depositoBuilder);
    }

}
