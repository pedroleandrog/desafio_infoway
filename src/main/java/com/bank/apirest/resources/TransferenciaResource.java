package com.bank.apirest.resources;

import com.bank.apirest.builder.SaqueBuilder;
import com.bank.apirest.builder.TransferenciaBuilder;
import com.bank.apirest.repository.AgenciaRepository;
import com.bank.apirest.repository.ClienteRepository;
import com.bank.apirest.repository.ContaRepository;
import com.bank.apirest.repository.SaqueRepository;
import com.bank.apirest.rest.DefaultResponse;
import com.bank.apirest.service.ContaService;
import com.bank.apirest.service.SaqueService;
import com.bank.apirest.service.TransferenciaService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/api")
@Api(value = "API Rest Trasferencia")
@CrossOrigin(origins = "*")
public class TransferenciaResource {

    @Autowired
    ContaRepository contaRepository;

    @Autowired
    AgenciaRepository agenciaRepository;

    @Autowired
    ClienteRepository clienteRepository;

    @Autowired
    SaqueRepository saqueRepository;

    @Autowired
    TransferenciaService transferenciaService;

    @PostMapping("/transferencia")
    @ApiOperation("Efetua uma operaçao de transferencia na conta informada")
    public DefaultResponse efetuarSaque(@RequestBody TransferenciaBuilder transferenciaBuilder) {
        return transferenciaService.save(transferenciaBuilder);
    }


}
