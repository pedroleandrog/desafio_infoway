package com.bank.apirest.resources;

import com.bank.apirest.builder.BancoBuilder;
import com.bank.apirest.models.Agencia;
import com.bank.apirest.models.Banco;
import com.bank.apirest.repository.AgenciaRepository;
import com.bank.apirest.repository.BancoRepository;
import com.bank.apirest.rest.DefaultResponse;
import com.bank.apirest.service.BancoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

@RestController
@RequestMapping(value = "/api")
@Api(value = "API Rest Banco")
@CrossOrigin(origins = "*")
public class BancoResource {

    @Autowired
    BancoRepository bancoRepository;

    @Autowired
    AgenciaRepository agenciaRepository;

    @Autowired
    BancoService bancoService;

    @GetMapping("/banco")
    @ApiOperation(value = "Retorna uma lista de bancos cadastrados")
    public List<Banco> listarBancos(){
        List<Banco> bancos = new ArrayList<Banco>();
        bancoRepository.findAll()
                .forEach(new Consumer<Banco>() {
                    @Override
                    public void accept(Banco banco) {
                        List<Agencia> agenciaList = agenciaRepository.findAgenciasByBanco(banco);

                        banco.setAgencias(agenciaList);
                        bancos.add(banco);
                    }
                });
        return bancos;
    }


    @GetMapping("/banco/{id}")
    @ApiOperation(value = "Retorna um banco unico")
    public Banco listarBancoUnico(@PathVariable(value = "id") long id){
        return bancoRepository.findById(id);
    }

    @PostMapping("/banco")
    @ApiOperation(value = "Cadastra uma nova instituiçao financeira")
    public DefaultResponse cadastrarBanco(@RequestBody BancoBuilder bancoBuilder){ ;
        return bancoService.save(bancoBuilder);
    }

    @DeleteMapping("/banco")
    @ApiOperation(value = "Deleta um banco")
    public DefaultResponse deletarBanco(@RequestParam long id){
        return bancoService.deleteById(id);
    }

    @PutMapping("/banco")
    @ApiOperation(value = "Atualiza as informaçoes do banco")
    public DefaultResponse atualizarBanco(@RequestParam long idBanco, @RequestBody BancoBuilder bancoBuilder){
        return bancoService.update(idBanco, bancoBuilder);
    }

}
