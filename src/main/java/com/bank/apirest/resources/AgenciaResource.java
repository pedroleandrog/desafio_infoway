package com.bank.apirest.resources;

import com.bank.apirest.builder.AgenciaBuilder;
import com.bank.apirest.models.Agencia;
import com.bank.apirest.models.Banco;
import com.bank.apirest.models.Conta;
import com.bank.apirest.repository.AgenciaRepository;
import com.bank.apirest.repository.BancoRepository;
import com.bank.apirest.repository.ContaRepository;
import com.bank.apirest.rest.DefaultResponse;
import com.bank.apirest.service.AgenciaService;
import com.bank.apirest.service.BancoService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

@RestController
@RequestMapping(value = "api")
@Api(value = "API Rest Agencia")
public class AgenciaResource {

    @Autowired
    AgenciaRepository agenciaRepository;

    @Autowired
    BancoRepository bancoRepository;

    @Autowired
    ContaRepository contaRepository;

    @Autowired
    AgenciaService agenciaService;

    @GetMapping("/agencia")
    @ApiOperation(value = "Retorna uma lista de agencias cadastradas")
    public List<Agencia> listarAgencias() {
        List<Agencia> agencias = new ArrayList<Agencia>();

        agenciaRepository.findAll()
                .forEach(agencias::add);

        return agencias;
    }

    //procurar agencia por banco

    @GetMapping("/agencia/{id}")
    @ApiOperation(value = "Retorna uma agencia unica")
    public Agencia listarAgenciaUnica(@PathVariable(value = "id") long id) {
        return agenciaRepository.findById(id);
    }

    @PostMapping("/agencia")
    @ApiOperation("Cadastra uma nova agencia bancaria em um banco ja cadastrado")
    public DefaultResponse cadastrarAgencia(@RequestBody AgenciaBuilder agenciaBuilder) {
        return agenciaService.save(agenciaBuilder);
    }

    @DeleteMapping("/agencia")
    @ApiOperation(value = "Deleta uma agencia")
    public DefaultResponse deletarAgencia(@RequestParam long id) {
        return agenciaService.deleteById(id);
    }

    @PutMapping("/agencia")
    @ApiOperation(value = "Atualiza as informaçoes da agencia")
    public DefaultResponse atualizarAgencia(@RequestParam long idBanco, @RequestBody Agencia agencia) {
        return agenciaService.update(idBanco, agencia);
    }

}
