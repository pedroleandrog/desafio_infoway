package com.bank.apirest.resources;

import com.bank.apirest.builder.ContaBuilder;
import com.bank.apirest.builder.SaqueBuilder;
import com.bank.apirest.models.Conta;
import com.bank.apirest.models.Saque;
import com.bank.apirest.repository.AgenciaRepository;
import com.bank.apirest.repository.ClienteRepository;
import com.bank.apirest.repository.ContaRepository;
import com.bank.apirest.repository.SaqueRepository;
import com.bank.apirest.rest.DefaultResponse;
import com.bank.apirest.service.ContaService;
import com.bank.apirest.service.SaqueService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(value = "/api")
@Api(value = "API Rest Saque")
@CrossOrigin(origins = "*")
public class SaqueResource {

    @Autowired
    ContaRepository contaRepository;

    @Autowired
    AgenciaRepository agenciaRepository;

    @Autowired
    ClienteRepository clienteRepository;

    @Autowired
    SaqueRepository saqueRepository;

    @Autowired
    ContaService contaService;

    @Autowired
    SaqueService saqueService;

    @PostMapping("/saque")
    @ApiOperation("Efetua uma operaçao de saque na conta informada")
    public DefaultResponse efetuarSaque(@RequestBody SaqueBuilder saqueBuilder) {
        return saqueService.save(saqueBuilder);
    }

}
