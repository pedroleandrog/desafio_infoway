package com.bank.apirest.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "TB_SAQUE")
public class Saque implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonIgnore
    private long id;

    @Column(nullable = false)
    @JsonIgnore
    private String numeroAgencia;

    @Column(nullable = false)
    @JsonIgnore
    private String numeroConta;

    @Column(nullable = false)
    private double valorSaque;

    @JoinColumn(nullable = false)
    @ManyToOne
    @JsonIgnore
    private Conta conta;

    public Saque() {
    }

    public Saque(String numeroAgencia, String numeroConta, double valorSaque, Conta conta){
        this.numeroAgencia = numeroAgencia;
        this.numeroConta = numeroConta;
        this.valorSaque = valorSaque;
        this.conta = conta;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNumeroAgencia() {
        return numeroAgencia;
    }

    public void setNumeroAgencia(String numeroAgencia) {
        this.numeroAgencia = numeroAgencia;
    }

    public String getNumeroConta() {
        return numeroConta;
    }

    public void setNumeroConta(String numeroConta) {
        this.numeroConta = numeroConta;
    }

    public double getValorSaque() {
        return valorSaque;
    }

    public void setValorSaque(double valorSacado) {
        this.valorSaque = valorSacado;
    }

    public Conta getConta() {
        return conta;
    }

    public void setConta(Conta conta) {
        this.conta = conta;
    }
}
