package com.bank.apirest.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "TB_TRANSFERENCIA")
public class Transferencia implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonIgnore
    private long id;

    @Column(nullable = false)
    @JsonIgnore
    private String numeroAgenciaOrigem;

    @Column(nullable = false)
    @JsonIgnore
    private String numeroContaOrigem;

    @Column(nullable = false)
    @JsonIgnore
    private String numeroAgenciaDestino;

    @Column(nullable = false)
    @JsonIgnore
    private String numeroContaDestino;

    @Column(nullable = false)
    private double valorTransferencia;

    @JoinColumn(nullable = false)
    @ManyToOne
    @JsonIgnore
    private Conta conta;

    public Transferencia() {
    }

    public Transferencia(String numeroAgenciaOrigem, String numeroContaOrigem, String numeroAgenciaDestino, String numeroContaDestino, double valorTransferencia, Conta conta){

        this.numeroAgenciaOrigem = numeroAgenciaOrigem;
        this.numeroContaOrigem = numeroContaOrigem;
        this.numeroAgenciaDestino = numeroAgenciaDestino;
        this.numeroContaDestino = numeroContaDestino;
        this.valorTransferencia = valorTransferencia;
        this.conta = conta;

    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNumeroAgenciaOrigem() {
        return numeroAgenciaOrigem;
    }

    public void setNumeroAgenciaOrigem(String numeroAgenciaOrigem) {
        this.numeroAgenciaOrigem = numeroAgenciaOrigem;
    }

    public String getNumeroContaOrigem() {
        return numeroContaOrigem;
    }

    public void setNumeroContaOrigem(String numeroContaOrigem) {
        this.numeroContaOrigem = numeroContaOrigem;
    }

    public String getNumeroAgenciaDestino() {
        return numeroAgenciaDestino;
    }

    public void setNumeroAgenciaDestino(String numeroAgencia) {
        this.numeroAgenciaDestino = numeroAgencia;
    }

    public String getNumeroContaDestino() {
        return numeroContaDestino;
    }

    public void setNumeroContaDestino(String numeroConta) {
        this.numeroContaDestino = numeroConta;
    }

    public double getValorTransferencia() {
        return valorTransferencia;
    }

    public void setValorTransferencia(double valorDeposito) {
        this.valorTransferencia = valorDeposito;
    }

    public Conta getConta() {
        return conta;
    }

    public void setConta(Conta conta) {
        this.conta = conta;
    }

}
