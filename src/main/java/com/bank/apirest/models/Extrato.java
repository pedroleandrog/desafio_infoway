//package com.bank.apirest.models;
//
//import com.fasterxml.jackson.annotation.JsonIgnore;
//
//import javax.persistence.*;
//import java.io.Serializable;
//import java.util.List;
//
//@Entity
//@Table(name = "TB_EXTRATO")
//public class Extrato implements Serializable {
//
//    private static final long serialVersionUID = 1L;
//
//    @Id
//    @GeneratedValue(strategy = GenerationType.IDENTITY)
//    private long id;
//
//    @OneToMany
//    @Column(nullable = false)
//    List<Deposito> depositoList;
//
//    @OneToMany
//    @Column(nullable = false)
//    List<Transferencia> transferenciaList;
//
//    @ManyToOne
//    @JoinColumn(nullable = false)
//    @JsonIgnore
//    private Conta conta;
//
//    public long getId() {
//        return id;
//    }
//
//    public void setId(long id) {
//        this.id = id;
//    }
//
//    public Conta getConta() {
//        return conta;
//    }
//
//    public void setConta(Conta conta) {
//        this.conta = conta;
//    }
//
//    public List<Deposito> getDepositoList() {
//        return depositoList;
//    }
//
//    public void setDepositoList(List<Deposito> depositoList) {
//        this.depositoList = depositoList;
//    }
//
//    public List<Transferencia> getTransferenciaList() {
//        return transferenciaList;
//    }
//
//    public void setTransferenciaList(List<Transferencia> transferenciaList) {
//        this.transferenciaList = transferenciaList;
//    }
//}
