package com.bank.apirest.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "TB_CONTA")
public class Conta implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @JoinColumn(nullable = false)
    @ManyToOne
    @JsonIgnore
    private Agencia agencia;

    @Column(nullable = false)
    private String numeroAgencia;

    @JoinColumn(nullable = false)
    @ManyToOne
    @JsonIgnore
    private Cliente cliente;

    @Column(nullable = false)
    private String numeroConta;

    @Column(nullable = false)
    private double saldo;

    @OneToMany
    @Column(nullable = false)
    private List<Saque> saques;

    @OneToMany
    @Column(nullable = false)
    private List<Deposito> depositos;

    @OneToMany
    @Column(nullable = false)
    private List<Transferencia> transferencias;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getNumeroAgencia() {
        return numeroAgencia;
    }

    public void setNumeroAgencia(String numeroAgencia) {
        this.numeroAgencia = numeroAgencia;
    }

    public Agencia getAgencia() {
        return agencia;
    }

    public void setAgencia(Agencia agencia) {
        this.agencia = agencia;
    }

    public String getNumeroConta() {
        return numeroConta;
    }

    public void setNumeroConta(String numeroConta) {
        this.numeroConta = numeroConta;
    }

    public double getSaldo() {
        return saldo;
    }

    public void setSaldo(double saldo) {
        this.saldo = saldo;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public List<Saque> getSaques() {
        return saques;
    }

    public void setSaques(List<Saque> saqueList) {
        this.saques = saqueList;
    }

    public List<Deposito> getDepositos() {
        return depositos;
    }

    public void setDepositos(List<Deposito> depositoList) {
        this.depositos = depositoList;
    }

    public List<Transferencia> getTransferencias() {
        return transferencias;
    }

    public void setTransferencias(List<Transferencia> transferencias) {
        this.transferencias = transferencias;
    }
}
