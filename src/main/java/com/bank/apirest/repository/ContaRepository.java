package com.bank.apirest.repository;

import com.bank.apirest.models.Agencia;
import com.bank.apirest.models.Cliente;
import com.bank.apirest.models.Conta;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface ContaRepository extends CrudRepository<Conta, Long> {

    Conta findById(long id);
    List<Conta> findContasByAgencia(Agencia agencia);
    List<Conta> findContasByCliente(Cliente cliente);
    Optional<Conta> findContaByNumeroConta(String numeroConta);

}
