package com.bank.apirest.repository;

import com.bank.apirest.models.Banco;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;


public interface BancoRepository extends CrudRepository<Banco, Long> {

    Banco findById(long id);

}
