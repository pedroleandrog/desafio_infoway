package com.bank.apirest.repository;

import com.bank.apirest.models.Cliente;
import org.springframework.data.repository.CrudRepository;

public interface ClienteRepository extends CrudRepository<Cliente, Long> {

    Cliente findById(long id);

}
