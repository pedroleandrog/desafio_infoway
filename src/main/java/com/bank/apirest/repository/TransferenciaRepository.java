package com.bank.apirest.repository;

import com.bank.apirest.models.Conta;
import com.bank.apirest.models.Transferencia;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface TransferenciaRepository extends CrudRepository<Transferencia, Long> {

    List<Transferencia> findTransferenciaByConta(Conta conta);

}
