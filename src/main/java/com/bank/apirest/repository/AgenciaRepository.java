package com.bank.apirest.repository;

import com.bank.apirest.models.Agencia;
import com.bank.apirest.models.Banco;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface AgenciaRepository extends CrudRepository<Agencia, Long> {

    Agencia findById(long id);
    List<Agencia> findAgenciasByBanco(Banco banco);
    Optional<Agencia> findAgenciaByNumeroAgencia(String numeroAgencia);

}
