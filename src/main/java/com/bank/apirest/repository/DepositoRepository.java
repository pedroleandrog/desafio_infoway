package com.bank.apirest.repository;

import com.bank.apirest.models.Conta;
import com.bank.apirest.models.Deposito;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface DepositoRepository extends CrudRepository<Deposito, Long> {

    List<Deposito> findDepositoByConta(Conta conta);

}
