package com.bank.apirest.repository;

import com.bank.apirest.models.Conta;
import com.bank.apirest.models.Saque;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface SaqueRepository extends CrudRepository<Saque, Long> {

    Saque findById(long id);
    List<Saque> findSaqueByConta(Conta conta);

}
