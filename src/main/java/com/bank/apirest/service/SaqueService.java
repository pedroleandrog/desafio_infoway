package com.bank.apirest.service;

import ch.qos.logback.core.pattern.parser.OptionTokenizer;
import com.bank.apirest.builder.ContaBuilder;
import com.bank.apirest.builder.SaqueBuilder;
import com.bank.apirest.models.*;
import com.bank.apirest.repository.*;
import com.bank.apirest.rest.DefaultResponse;
import com.bank.apirest.rest.ExceptionHandling;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class SaqueService {

    @Autowired
    AgenciaRepository agenciaRepository;

    @Autowired
    ContaRepository contaRepository;

    @Autowired
    SaqueRepository saqueRepository;

    ExceptionHandling exceptionHandling;

    public DefaultResponse save(SaqueBuilder saqueBuilder) {

        Saque saque = new Saque();

        //verificar se a agencia existe

        Optional<Agencia> agencia = agenciaRepository.findAgenciaByNumeroAgencia(saqueBuilder.getNumeroAgencia());
        Optional<Conta> conta = contaRepository.findContaByNumeroConta(saqueBuilder.getNumeroConta());

        if(!agencia.isPresent()){
            return DefaultResponse.buildWithError("NAO FOI POSSIVEL EFETUAR O SAQUE", "Agencia Nao Existe");
        }else{
            saque.setNumeroAgencia(saqueBuilder.getNumeroAgencia());
        }

        //verificar se a conta existe

        if(!conta.isPresent()){
            return DefaultResponse.buildWithError("NAO FOI POSSIVEL EFETUAR O SAQUE", "Conta Nao Existe");
        }else{
            saque.setNumeroConta(saqueBuilder.getNumeroConta());
            saque.setConta(conta.get());

        }

        //verificar se o valor do saque eh maior que o valor na conta

        if (saqueBuilder.getValorSaque() > conta.get().getSaldo()) {
            return DefaultResponse.buildWithError("NAO FOI POSSIVEL EFETUAR O SAQUE", "Saldo Insuficiente na Conta");
        } else {
            saque.setValorSaque(saqueBuilder.getValorSaque());
            double novoSaldo = conta.get().getSaldo() - saqueBuilder.getValorSaque();
            conta.get().setSaldo(novoSaldo);
        }

        try {

            saqueRepository.save(saque);

        } catch (Exception e) {
            exceptionHandling = new ExceptionHandling(e, "Agencia ou Conta Nao Existem");
            System.out.println(e);
            System.out.println(exceptionHandling.errorHandling());
            return DefaultResponse.buildWithError("NAO FOI POSSIVEL EFETUAR O SAQUE", exceptionHandling.getMensagem());
        }

        return DefaultResponse.build(saque);
    }
}
