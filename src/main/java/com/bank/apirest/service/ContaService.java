package com.bank.apirest.service;

import com.bank.apirest.builder.ContaBuilder;
import com.bank.apirest.models.Agencia;
import com.bank.apirest.models.Cliente;
import com.bank.apirest.models.Conta;
import com.bank.apirest.repository.AgenciaRepository;
import com.bank.apirest.repository.BancoRepository;
import com.bank.apirest.repository.ClienteRepository;
import com.bank.apirest.repository.ContaRepository;
import com.bank.apirest.rest.DefaultResponse;
import com.bank.apirest.rest.ExceptionHandling;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ContaService {

    @Autowired
    AgenciaRepository agenciaRepository;

    @Autowired
    BancoRepository bancoRepository;

    @Autowired
    ContaRepository contaRepository;

    @Autowired
    ClienteRepository clienteRepository;

    ExceptionHandling exceptionHandling;

    public DefaultResponse save(ContaBuilder contaBuilder) {

        Conta conta = new Conta();
        Agencia agencia = agenciaRepository.findById(contaBuilder.getIdAgencia());
        Cliente cliente = clienteRepository.findById(contaBuilder.getIdCliente());

        conta.setAgencia(agencia);
        conta.setCliente(cliente);
        conta.setNumeroConta(contaBuilder.getNumeroConta());
        conta.setSaldo(contaBuilder.getSaldo());

        try {
            conta.setNumeroAgencia(conta.getAgencia().getNumeroAgencia());
            contaRepository.save(conta);

        } catch (Exception e) {
            exceptionHandling = new ExceptionHandling(e, "Necessario Criar uma Agencia e/ou Um Cliente");
            System.out.println(exceptionHandling.errorHandling());
            return DefaultResponse.buildWithError("NAO FOI POSSIVEL CADASTRAR A CONTA", exceptionHandling.getMensagem());
        }

        return DefaultResponse.build(conta);
    }

    public DefaultResponse update(Conta conta) {

        try {
            contaRepository.save(conta);

        } catch (Exception e) {
            exceptionHandling = new ExceptionHandling(e, "Necessario Criar uma Conta");
            System.out.println(exceptionHandling.errorHandling());
            return DefaultResponse.buildWithError("NAO FOI POSSIVEL ATUALIZAR A CONTA", exceptionHandling.getMensagem());
        }

        return DefaultResponse.build(conta);
    }

    public DefaultResponse deleteById(long id) {

        try {
            contaRepository.deleteById(id);
        } catch (Exception e) {
            ExceptionHandling exceptionHandling = new ExceptionHandling(e, "Necessario Apagar os Clientes");
            System.out.println(exceptionHandling.errorHandling());
            return DefaultResponse.buildWithError("Nao Foi Possivel Deletar a Conta", exceptionHandling.getMensagem());
        }
        return DefaultResponse.build("EXCLUIDO COM SUCESSO");
    }

}
