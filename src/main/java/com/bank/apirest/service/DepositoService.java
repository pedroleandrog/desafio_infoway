package com.bank.apirest.service;

import com.bank.apirest.builder.DepositoBuilder;
import com.bank.apirest.models.Agencia;
import com.bank.apirest.models.Conta;
import com.bank.apirest.models.Deposito;
import com.bank.apirest.repository.AgenciaRepository;
import com.bank.apirest.repository.ContaRepository;
import com.bank.apirest.repository.DepositoRepository;
import com.bank.apirest.rest.DefaultResponse;
import com.bank.apirest.rest.ExceptionHandling;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class DepositoService {

    @Autowired
    AgenciaRepository agenciaRepository;

    @Autowired
    ContaRepository contaRepository;

    @Autowired
    DepositoRepository depositoRepository;

    ExceptionHandling exceptionHandling;

    public DefaultResponse save(DepositoBuilder depositoBuilder){

        Deposito deposito = new Deposito();

        //verificar se a agencia existe

        Optional<Agencia> agencia = agenciaRepository.findAgenciaByNumeroAgencia(depositoBuilder.getNumeroAgencia());
        Optional<Conta> conta = contaRepository.findContaByNumeroConta(depositoBuilder.getNumeroConta());

        if(!agencia.isPresent()){
            return DefaultResponse.buildWithError("NAO FOI POSSIVEL EFETUAR O DEPOSITO", "Agencia Nao Existe");
        }else{
            deposito.setNumeroAgencia(depositoBuilder.getNumeroAgencia());
        }

        //verificar se a conta existe

        if(!conta.isPresent()){
            return DefaultResponse.buildWithError("NAO FOI POSSIVEL EFETUAR O DEPOSITO", "Conta Nao Existe");
        }else{
            deposito.setNumeroConta(depositoBuilder.getNumeroConta());
            deposito.setConta(conta.get());
        }

        // Verifica se o valor do deposito eh valido para ser depositado

        if (depositoBuilder.getValorDeposito() < 0) {
            return DefaultResponse.buildWithError("NAO FOI POSSIVEL EFETUAR O DEPOSITO", "Valor Invalido Para Deposito");
        } else {
            deposito.setValorDeposito(depositoBuilder.getValorDeposito());
            double novoSaldo = deposito.getValorDeposito() + conta.get().getSaldo();
            conta.get().setSaldo(novoSaldo);
        }

        try {

            depositoRepository.save(deposito);

        } catch (Exception e) {
            exceptionHandling = new ExceptionHandling(e, "Agencia ou Conta Nao Existem");
            System.out.println(e);
            System.out.println(exceptionHandling.errorHandling());
            return DefaultResponse.buildWithError("NAO FOI POSSIVEL EFETUAR O DEPOSITO", exceptionHandling.getMensagem());
        }



        return DefaultResponse.build(deposito);

    }
}
