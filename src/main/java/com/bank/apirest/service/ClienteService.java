package com.bank.apirest.service;

import com.bank.apirest.builder.BancoBuilder;
import com.bank.apirest.builder.ClienteBuilder;
import com.bank.apirest.models.Agencia;
import com.bank.apirest.models.Banco;
import com.bank.apirest.models.Cliente;
import com.bank.apirest.repository.AgenciaRepository;
import com.bank.apirest.repository.BancoRepository;
import com.bank.apirest.repository.ClienteRepository;
import com.bank.apirest.repository.ContaRepository;
import com.bank.apirest.rest.DefaultResponse;
import com.bank.apirest.rest.ExceptionHandling;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ClienteService {

    @Autowired
    AgenciaRepository agenciaRepository;

    @Autowired
    BancoRepository bancoRepository;

    @Autowired
    ContaRepository contaRepository;

    @Autowired
    ClienteRepository clienteRepository;

    ExceptionHandling exceptionHandling;

    public DefaultResponse save(ClienteBuilder clienteBuilder) {

        Cliente cliente = new Cliente();
        cliente.setNome(clienteBuilder.getNome());
        cliente.setCpf(clienteBuilder.getCpf());

        try {

            clienteRepository.save(cliente);
        } catch (Exception e) {
            exceptionHandling = new ExceptionHandling(e, "Argumentos Invalidos");
            System.out.println(exceptionHandling.errorHandling());
            return DefaultResponse.buildWithError("Nao Foi Possivel Cadastrar O Cliente", exceptionHandling.getMensagem());
        }
        return DefaultResponse.build(cliente);
    }

    public DefaultResponse update(long idCliente, ClienteBuilder clienteBuilder) {

        try {
            Cliente cliente = clienteRepository.findById(idCliente);
            cliente.setNome(clienteBuilder.getNome());
            cliente.setCpf(clienteBuilder.getCpf());
            clienteRepository.save(cliente);
        } catch (Exception e) {
            exceptionHandling = new ExceptionHandling(e, "Necessario Criar um Cliente");
            System.out.println(exceptionHandling.errorHandling());
            return DefaultResponse.buildWithError("NAO FOI POSSIVEL ATUALIZAR O CLIENTE", exceptionHandling.getMensagem());
        }

        return DefaultResponse.build(clienteBuilder);
    }

    public DefaultResponse deleteById(long id) {

        try {
            clienteRepository.deleteById(id);
        } catch (Exception e) {
            exceptionHandling = new ExceptionHandling(e, "Cliente Nao Encontrado");
            System.out.println(exceptionHandling.errorHandling());
            return DefaultResponse.buildWithError("Nao Foi Possivel Deletar o Cliente", exceptionHandling.getMensagem());
        }
        return DefaultResponse.build("EXCLUIDO COM SUCESSO");
    }

}
