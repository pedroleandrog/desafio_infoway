package com.bank.apirest.service;

import com.bank.apirest.builder.AgenciaBuilder;
import com.bank.apirest.models.Agencia;
import com.bank.apirest.models.Banco;
import com.bank.apirest.models.Conta;
import com.bank.apirest.repository.AgenciaRepository;
import com.bank.apirest.repository.BancoRepository;
import com.bank.apirest.repository.ContaRepository;
import com.bank.apirest.rest.DefaultResponse;
import com.bank.apirest.rest.ExceptionHandling;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AgenciaService {


    @Autowired
    AgenciaRepository agenciaRepository;

    @Autowired
    BancoRepository bancoRepository;

    @Autowired
    ContaRepository contaRepository;

    ExceptionHandling exceptionHandling;

    public DefaultResponse save(AgenciaBuilder agenciaBuilder) {

        Agencia agencia = new Agencia();
        agencia.setNumeroAgencia(agenciaBuilder.getNumeroAgencia());

        Banco banco = bancoRepository.findById(agenciaBuilder.getIdBanco());
        agencia.setBanco(banco);

        try {
            agenciaRepository.save(agencia);
        } catch (Exception e) {
            exceptionHandling = new ExceptionHandling(e, "Necessario Criar um Banco");
            System.out.println(exceptionHandling.errorHandling());
            return DefaultResponse.buildWithError("Nao Foi Possivel Cadastrar Agencia", exceptionHandling.getMensagem());
        }
        return DefaultResponse.build(agencia);
    }

    public DefaultResponse update(long idBanco, Agencia agencia) {

        try {
            Banco banco = bancoRepository.findById(idBanco);
            agencia.setBanco(banco);
            agenciaRepository.save(agencia);
        } catch (Exception e) {
            exceptionHandling = new ExceptionHandling(e, "Necessario Criar uma Agencia");
            System.out.println(exceptionHandling.errorHandling());
            return DefaultResponse.buildWithError("NAO FOI POSSIVEL ATUALIZAR A AGENCIA", exceptionHandling.getMensagem());
        }

        return DefaultResponse.build(agencia);
    }

    public DefaultResponse deleteById(long id) {

        try {
            agenciaRepository.deleteById(id);
        } catch (Exception e) {
            ExceptionHandling exceptionHandling = new ExceptionHandling(e, "Necessario Apagar as Contas");
            System.out.println(exceptionHandling.errorHandling());
            return DefaultResponse.buildWithError("Nao Foi Possivel Deletar a Agencia", exceptionHandling.getMensagem());
        }
        return DefaultResponse.build("EXCLUIDO COM SUCESSO");
    }

}
