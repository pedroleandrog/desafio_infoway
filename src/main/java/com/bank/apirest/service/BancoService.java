package com.bank.apirest.service;

import com.bank.apirest.builder.AgenciaBuilder;
import com.bank.apirest.builder.BancoBuilder;
import com.bank.apirest.models.Agencia;
import com.bank.apirest.models.Banco;
import com.bank.apirest.repository.AgenciaRepository;
import com.bank.apirest.repository.BancoRepository;
import com.bank.apirest.repository.ContaRepository;
import com.bank.apirest.rest.DefaultResponse;
import com.bank.apirest.rest.ExceptionHandling;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BancoService {

    @Autowired
    AgenciaRepository agenciaRepository;

    @Autowired
    BancoRepository bancoRepository;

    @Autowired
    ContaRepository contaRepository;

    ExceptionHandling exceptionHandling;

    public DefaultResponse save(BancoBuilder bancoBuilder) {

        Banco banco = new Banco();
        banco.setNome(bancoBuilder.getNome());

        try {
            bancoRepository.save(banco);
        } catch (Exception e) {
            exceptionHandling = new ExceptionHandling(e, "Argumentos Invalidos");
            System.out.println(exceptionHandling.errorHandling());
            return DefaultResponse.buildWithError("Nao Foi Possivel Cadastrar O Banco", exceptionHandling.getMensagem());
        }
        return DefaultResponse.build(banco);
    }

    public DefaultResponse update(long idBanco, BancoBuilder bancoBuilder) {

        Banco banco;

        try {
            banco = bancoRepository.findById(idBanco);

            banco.setNome(bancoBuilder.getNome());

            bancoRepository.save(banco);
        } catch (Exception e) {
            exceptionHandling = new ExceptionHandling(e, "Necessario Criar uma Agencia");
            System.out.println(exceptionHandling.errorHandling());
            return DefaultResponse.buildWithError("NAO FOI POSSIVEL ATUALIZAR A AGENCIA", exceptionHandling.getMensagem());
        }

        return DefaultResponse.build(banco);
    }

    public DefaultResponse deleteById(long id) {

        try {
            bancoRepository.deleteById(id);
        } catch (Exception e) {
            ExceptionHandling exceptionHandling = new ExceptionHandling(e, "Necessario Apagar as Agencias");
            System.out.println(exceptionHandling.errorHandling());
            return DefaultResponse.buildWithError("Nao Foi Possivel Deletar o Banco", exceptionHandling.getMensagem());
        }
        return DefaultResponse.build("EXCLUIDO COM SUCESSO");
    }
}
