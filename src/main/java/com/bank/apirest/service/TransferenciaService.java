package com.bank.apirest.service;

import com.bank.apirest.builder.TransferenciaBuilder;
import com.bank.apirest.models.Agencia;
import com.bank.apirest.models.Conta;
import com.bank.apirest.models.Transferencia;
import com.bank.apirest.repository.AgenciaRepository;
import com.bank.apirest.repository.ContaRepository;
import com.bank.apirest.repository.TransferenciaRepository;
import com.bank.apirest.rest.DefaultResponse;
import com.bank.apirest.rest.ExceptionHandling;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class TransferenciaService {

    @Autowired
    AgenciaRepository agenciaRepository;

    @Autowired
    ContaRepository contaRepository;

    @Autowired
    TransferenciaRepository transferenciaRepository;

    ExceptionHandling exceptionHandling;

    public DefaultResponse save(TransferenciaBuilder transferenciaBuilder) {

        Transferencia transferencia = new Transferencia();

        //verificar se a agencia existe

        Optional<Agencia> agenciaOrigem = agenciaRepository.findAgenciaByNumeroAgencia(transferenciaBuilder.getNumeroAgenciaOrigem());
        Optional<Conta> contaOrigem = contaRepository.findContaByNumeroConta(transferenciaBuilder.getNumeroContaOrigem());

        Optional<Agencia> agenciaDestino = agenciaRepository.findAgenciaByNumeroAgencia(transferenciaBuilder.getNumeroAgenciaDestino());
        Optional<Conta> contaDestino = contaRepository.findContaByNumeroConta(transferenciaBuilder.getNumeroContaDestino());

        if(!agenciaOrigem.isPresent()){
            return DefaultResponse.buildWithError("NAO FOI POSSIVEL EFETUAR A TRANSFERENCIA", "Agencia de Origem Nao Existe");
        }else{
            transferencia.setNumeroAgenciaOrigem(transferenciaBuilder.getNumeroAgenciaDestino());
        }

        if(!agenciaDestino.isPresent()){
            return DefaultResponse.buildWithError("NAO FOI POSSIVEL EFETUAR A TRANSFERENCIA", "Agencia de Destino Nao Existe");
        }else{
            transferencia.setNumeroAgenciaDestino(transferenciaBuilder.getNumeroAgenciaOrigem());
        }

        //verificar se a conta existe

        if(!contaOrigem.isPresent()){
            return DefaultResponse.buildWithError("NAO FOI POSSIVEL EFETUAR A TRANSFERENCIA", "Conta de Origem Nao Existe");
        }else{
            transferencia.setNumeroContaOrigem(transferenciaBuilder.getNumeroContaOrigem());
        }

        if(!contaDestino.isPresent()){
            return DefaultResponse.buildWithError("NAO FOI POSSIVEL EFETUAR A TRANSFERENCIA", "Conta de Destino Nao Existe");
        }else{
            transferencia.setNumeroContaDestino(transferenciaBuilder.getNumeroContaDestino());
            transferencia.setConta(contaDestino.get());
        }

        //verificar se o valor da transferencia eh maior que o valor na conta

        if (transferenciaBuilder.getValorTransferencia() > contaOrigem.get().getSaldo()) {
            return DefaultResponse.buildWithError("NAO FOI POSSIVEL EFETUAR A TRANSFERENCIA", "Saldo Insuficiente na Conta de Origem Para Transferencia");
        } else if(transferenciaBuilder.getValorTransferencia() < 0){
            return DefaultResponse.buildWithError("NAO FOI POSSIVEL EFETUAR O DEPOSITO", "Valor Invalido Para Transferencia");
        }else {
            transferencia.setValorTransferencia(transferenciaBuilder.getValorTransferencia());
            double novoSaldoDaContaOrigem = contaOrigem.get().getSaldo() - transferenciaBuilder.getValorTransferencia();
            contaOrigem.get().setSaldo(novoSaldoDaContaOrigem);


            double novoSaldoDaContaDestino = contaDestino.get().getSaldo() + transferenciaBuilder.getValorTransferencia();
            contaDestino.get().setSaldo(novoSaldoDaContaDestino);
        }

        try {

            transferenciaRepository.save(transferencia);

        } catch (Exception e) {
            exceptionHandling = new ExceptionHandling(e, "Agencia ou Conta Nao Existem");
            System.out.println(e);
            System.out.println(exceptionHandling.errorHandling());
            return DefaultResponse.buildWithError("NAO FOI POSSIVEL EFETUAR O SAQUE", exceptionHandling.getMensagem());
        }

        return DefaultResponse.build(transferencia);
    }

}
