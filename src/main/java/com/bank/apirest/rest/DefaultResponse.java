package com.bank.apirest.rest;

import javax.validation.constraints.Null;


public class DefaultResponse {

    private DefaultStatusResponse status;
    private String code;
    private String message;
    private Object data;

    private DefaultResponse(){};

    public static DefaultResponse buildWithError(String code, String  message) {
        DefaultResponse dr = new DefaultResponse();
        dr.status = DefaultStatusResponse.ERROR;
        dr.code = code;
        dr.message = message;
        dr.data = null;
        return dr;
    }

    public static DefaultResponse build(Object data) {
        DefaultResponse dr = new DefaultResponse();
        dr.status = DefaultStatusResponse.OK;
        dr.code = null;
        dr.message = null;
        dr.data = data;
        return dr;
    }

    public DefaultStatusResponse getStatus() {
        return status;
    }

    public void setStatus(DefaultStatusResponse status) {
        this.status = status;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
