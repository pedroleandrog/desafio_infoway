package com.bank.apirest.rest;

import org.springframework.dao.DataIntegrityViolationException;

public class ExceptionHandling {

    private Exception e;
    private String mensagem;

    public ExceptionHandling(Exception e, String mensagem) {
        this.e = e;
        this.mensagem = mensagem;
    }

    public String errorHandling(){
        if (DataIntegrityViolationException.class.equals(this.e.getClass())) {
            return mensagem;
        }
        if (NullPointerException.class.equals(this.e.getClass())){
            return mensagem;
        }
        return "Ocorreu um erro, mas nao pode ser identificado";
    }

    public Exception getE() {
        return e;
    }

    public void setE(Exception e) {
        this.e = e;
    }

    public String getMensagem() {
        return mensagem;
    }

    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }
}
