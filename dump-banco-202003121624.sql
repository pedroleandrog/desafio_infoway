-- MySQL dump 10.13  Distrib 5.7.29, for Linux (x86_64)
--
-- Host: localhost    Database: banco
-- ------------------------------------------------------
-- Server version	5.7.29-0ubuntu0.18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tb_agencia`
--

DROP TABLE IF EXISTS `tb_agencia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_agencia` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `numero_agencia` varchar(255) NOT NULL,
  `banco_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FKl4yfopdnw88hcmx10k5o0xy4c` (`banco_id`),
  CONSTRAINT `FKl4yfopdnw88hcmx10k5o0xy4c` FOREIGN KEY (`banco_id`) REFERENCES `tb_banco` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_agencia`
--

LOCK TABLES `tb_agencia` WRITE;
/*!40000 ALTER TABLE `tb_agencia` DISABLE KEYS */;
INSERT INTO `tb_agencia` VALUES (1,'0124-4',1),(2,'5545-9',2);
/*!40000 ALTER TABLE `tb_agencia` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_banco`
--

DROP TABLE IF EXISTS `tb_banco`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_banco` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_banco`
--

LOCK TABLES `tb_banco` WRITE;
/*!40000 ALTER TABLE `tb_banco` DISABLE KEYS */;
INSERT INTO `tb_banco` VALUES (1,'BANCO DA INFOWAY'),(2,'BANCO DA MAIDA.HEALTH'),(3,'BANCO DO BRASIL');
/*!40000 ALTER TABLE `tb_banco` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_banco_agencia_list`
--

DROP TABLE IF EXISTS `tb_banco_agencia_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_banco_agencia_list` (
  `banco_id` bigint(20) NOT NULL,
  `agencia_list_id` bigint(20) NOT NULL,
  UNIQUE KEY `UK_svqh4kic75i5a4xvjxtdrdaws` (`agencia_list_id`),
  KEY `FKqy2iwqjwt2cv77dtlhj1r9ybj` (`banco_id`),
  CONSTRAINT `FK19tja2mgswyakt3g2wspg9j38` FOREIGN KEY (`agencia_list_id`) REFERENCES `tb_agencia` (`id`),
  CONSTRAINT `FKqy2iwqjwt2cv77dtlhj1r9ybj` FOREIGN KEY (`banco_id`) REFERENCES `tb_banco` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_banco_agencia_list`
--

LOCK TABLES `tb_banco_agencia_list` WRITE;
/*!40000 ALTER TABLE `tb_banco_agencia_list` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_banco_agencia_list` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_banco_agencias`
--

DROP TABLE IF EXISTS `tb_banco_agencias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_banco_agencias` (
  `banco_id` bigint(20) NOT NULL,
  `agencias_id` bigint(20) NOT NULL,
  UNIQUE KEY `UK_qceeqvmchbcl6g6o0pyh2aolt` (`agencias_id`),
  KEY `FKfkg1fxvg3kp90hvmgobwp28va` (`banco_id`),
  CONSTRAINT `FK2ur4iip1waka89ovmggqtf2m0` FOREIGN KEY (`agencias_id`) REFERENCES `tb_agencia` (`id`),
  CONSTRAINT `FKfkg1fxvg3kp90hvmgobwp28va` FOREIGN KEY (`banco_id`) REFERENCES `tb_banco` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_banco_agencias`
--

LOCK TABLES `tb_banco_agencias` WRITE;
/*!40000 ALTER TABLE `tb_banco_agencias` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_banco_agencias` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_cliente`
--

DROP TABLE IF EXISTS `tb_cliente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_cliente` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `cpf` varchar(255) NOT NULL,
  `nome` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_cliente`
--

LOCK TABLES `tb_cliente` WRITE;
/*!40000 ALTER TABLE `tb_cliente` DISABLE KEYS */;
INSERT INTO `tb_cliente` VALUES (1,'398.590.510-03','PEDRO LEANDRO GOMES DA SILVA'),(2,'039.356.448-4','José Roberto Oliver Aparício'),(3,'725.860.230-56','JUAN CARLOS LIMA DOS SANTOS'),(4,'083.073.700-60','AIRTON SOUSA ARAUJO'),(5,'575.554.260-05','GERSON JAMES GUIMARAES MARTINS FERREIRA');
/*!40000 ALTER TABLE `tb_cliente` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_cliente_conta_list`
--

DROP TABLE IF EXISTS `tb_cliente_conta_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_cliente_conta_list` (
  `cliente_id` bigint(20) NOT NULL,
  `conta_list_id` bigint(20) NOT NULL,
  UNIQUE KEY `UK_84whxw79plfopw3yffx56uqmw` (`conta_list_id`),
  KEY `FK7hvybb1rwbmggco7d5tj4mkmq` (`cliente_id`),
  CONSTRAINT `FK7hvybb1rwbmggco7d5tj4mkmq` FOREIGN KEY (`cliente_id`) REFERENCES `tb_cliente` (`id`),
  CONSTRAINT `FKh167kxuyeccvrrna5vc4sea41` FOREIGN KEY (`conta_list_id`) REFERENCES `tb_conta` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_cliente_conta_list`
--

LOCK TABLES `tb_cliente_conta_list` WRITE;
/*!40000 ALTER TABLE `tb_cliente_conta_list` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_cliente_conta_list` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_cliente_contas`
--

DROP TABLE IF EXISTS `tb_cliente_contas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_cliente_contas` (
  `cliente_id` bigint(20) NOT NULL,
  `contas_id` bigint(20) NOT NULL,
  UNIQUE KEY `UK_2rcyo4e121ikxg9nmp5oxiy57` (`contas_id`),
  KEY `FK1wf782askhlgprkjd9dswrkm8` (`cliente_id`),
  CONSTRAINT `FK1wf782askhlgprkjd9dswrkm8` FOREIGN KEY (`cliente_id`) REFERENCES `tb_cliente` (`id`),
  CONSTRAINT `FKllp1iq1psf5pcmnuq9iip645x` FOREIGN KEY (`contas_id`) REFERENCES `tb_conta` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_cliente_contas`
--

LOCK TABLES `tb_cliente_contas` WRITE;
/*!40000 ALTER TABLE `tb_cliente_contas` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_cliente_contas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_conta`
--

DROP TABLE IF EXISTS `tb_conta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_conta` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `numero_agencia` varchar(255) NOT NULL,
  `numero_conta` varchar(255) NOT NULL,
  `saldo` double NOT NULL,
  `agencia_id` bigint(20) NOT NULL,
  `cliente_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FKr1pq5q75t3rqwsnq3ja9l9da7` (`agencia_id`),
  KEY `FK9o1x8ukef2hxvck5nrbavv12b` (`cliente_id`),
  CONSTRAINT `FK9o1x8ukef2hxvck5nrbavv12b` FOREIGN KEY (`cliente_id`) REFERENCES `tb_cliente` (`id`),
  CONSTRAINT `FKr1pq5q75t3rqwsnq3ja9l9da7` FOREIGN KEY (`agencia_id`) REFERENCES `tb_agencia` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_conta`
--

LOCK TABLES `tb_conta` WRITE;
/*!40000 ALTER TABLE `tb_conta` DISABLE KEYS */;
INSERT INTO `tb_conta` VALUES (1,'0124-4','60694-4',3050,1,1),(2,'0124-4','6722-4',1800,1,2),(3,'5545-9','14883-5',2750.89,2,3),(4,'5545-9','96306-2',0.12,2,4),(5,'5545-9','1123202-1',12000,2,5);
/*!40000 ALTER TABLE `tb_conta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_conta_deposito_list`
--

DROP TABLE IF EXISTS `tb_conta_deposito_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_conta_deposito_list` (
  `conta_id` bigint(20) NOT NULL,
  `deposito_list_id` bigint(20) NOT NULL,
  UNIQUE KEY `UK_owivke0qwpix9hui97h7h11el` (`deposito_list_id`),
  KEY `FK8q7k3q2qgt5jtyrvchtloylia` (`conta_id`),
  CONSTRAINT `FK8q7k3q2qgt5jtyrvchtloylia` FOREIGN KEY (`conta_id`) REFERENCES `tb_conta` (`id`),
  CONSTRAINT `FKq2ul0blx4clb3itp8oqmvjesa` FOREIGN KEY (`deposito_list_id`) REFERENCES `tb_deposito` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_conta_deposito_list`
--

LOCK TABLES `tb_conta_deposito_list` WRITE;
/*!40000 ALTER TABLE `tb_conta_deposito_list` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_conta_deposito_list` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_conta_depositos`
--

DROP TABLE IF EXISTS `tb_conta_depositos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_conta_depositos` (
  `conta_id` bigint(20) NOT NULL,
  `depositos_id` bigint(20) NOT NULL,
  UNIQUE KEY `UK_qbvts7a7m8ce6mntx2pt3u2s8` (`depositos_id`),
  KEY `FKel06sylf2ua6ssnpdsmkbgr3o` (`conta_id`),
  CONSTRAINT `FKel06sylf2ua6ssnpdsmkbgr3o` FOREIGN KEY (`conta_id`) REFERENCES `tb_conta` (`id`),
  CONSTRAINT `FKk8v57o41f2b3qysxmjol4o5l2` FOREIGN KEY (`depositos_id`) REFERENCES `tb_deposito` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_conta_depositos`
--

LOCK TABLES `tb_conta_depositos` WRITE;
/*!40000 ALTER TABLE `tb_conta_depositos` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_conta_depositos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_conta_saque_list`
--

DROP TABLE IF EXISTS `tb_conta_saque_list`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_conta_saque_list` (
  `conta_id` bigint(20) NOT NULL,
  `saque_list_id` bigint(20) NOT NULL,
  UNIQUE KEY `UK_8flmu5mstgsa1ajh9b23kfqv8` (`saque_list_id`),
  KEY `FKqaff1q4fd8176trj5awpjqxir` (`conta_id`),
  CONSTRAINT `FKqaff1q4fd8176trj5awpjqxir` FOREIGN KEY (`conta_id`) REFERENCES `tb_conta` (`id`),
  CONSTRAINT `FKry5ifu9ogu25t5ndcgcbsce24` FOREIGN KEY (`saque_list_id`) REFERENCES `tb_saque` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_conta_saque_list`
--

LOCK TABLES `tb_conta_saque_list` WRITE;
/*!40000 ALTER TABLE `tb_conta_saque_list` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_conta_saque_list` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_conta_saques`
--

DROP TABLE IF EXISTS `tb_conta_saques`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_conta_saques` (
  `conta_id` bigint(20) NOT NULL,
  `saques_id` bigint(20) NOT NULL,
  UNIQUE KEY `UK_1pe8fecnxj5iwu14l5jhopl6t` (`saques_id`),
  KEY `FKp3qxfv0nutkn7h8erk9h2tcp2` (`conta_id`),
  CONSTRAINT `FKp3qxfv0nutkn7h8erk9h2tcp2` FOREIGN KEY (`conta_id`) REFERENCES `tb_conta` (`id`),
  CONSTRAINT `FKq1v7bjqxh7w21s5v3qht6jhks` FOREIGN KEY (`saques_id`) REFERENCES `tb_saque` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_conta_saques`
--

LOCK TABLES `tb_conta_saques` WRITE;
/*!40000 ALTER TABLE `tb_conta_saques` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_conta_saques` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_conta_transferencias`
--

DROP TABLE IF EXISTS `tb_conta_transferencias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_conta_transferencias` (
  `conta_id` bigint(20) NOT NULL,
  `transferencias_id` bigint(20) NOT NULL,
  UNIQUE KEY `UK_ot9wsocjr7f0leslgryqms0ut` (`transferencias_id`),
  KEY `FKaxuti3rtqgxocrpiaa1gv6egk` (`conta_id`),
  CONSTRAINT `FKaxuti3rtqgxocrpiaa1gv6egk` FOREIGN KEY (`conta_id`) REFERENCES `tb_conta` (`id`),
  CONSTRAINT `FKpo0ouy2w5p17s148vs20i1bnn` FOREIGN KEY (`transferencias_id`) REFERENCES `tb_transferencia` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_conta_transferencias`
--

LOCK TABLES `tb_conta_transferencias` WRITE;
/*!40000 ALTER TABLE `tb_conta_transferencias` DISABLE KEYS */;
/*!40000 ALTER TABLE `tb_conta_transferencias` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_deposito`
--

DROP TABLE IF EXISTS `tb_deposito`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_deposito` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `numero_agencia` varchar(255) NOT NULL,
  `numero_conta` varchar(255) NOT NULL,
  `valor_deposito` double NOT NULL,
  `conta_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FKp79rbahkkjr4fidl2falisarm` (`conta_id`),
  CONSTRAINT `FKp79rbahkkjr4fidl2falisarm` FOREIGN KEY (`conta_id`) REFERENCES `tb_conta` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_deposito`
--

LOCK TABLES `tb_deposito` WRITE;
/*!40000 ALTER TABLE `tb_deposito` DISABLE KEYS */;
INSERT INTO `tb_deposito` VALUES (1,'0124-4','60694-4',500,1),(2,'0124-4','60694-4',350,1);
/*!40000 ALTER TABLE `tb_deposito` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_saque`
--

DROP TABLE IF EXISTS `tb_saque`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_saque` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `numero_agencia` varchar(255) NOT NULL,
  `numero_conta` varchar(255) NOT NULL,
  `valor_saque` double NOT NULL,
  `conta_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK80qg1hpegn7h1q8c5v47qgpsf` (`conta_id`),
  CONSTRAINT `FK80qg1hpegn7h1q8c5v47qgpsf` FOREIGN KEY (`conta_id`) REFERENCES `tb_conta` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_saque`
--

LOCK TABLES `tb_saque` WRITE;
/*!40000 ALTER TABLE `tb_saque` DISABLE KEYS */;
INSERT INTO `tb_saque` VALUES (1,'0124-4','60694-4',200,1);
/*!40000 ALTER TABLE `tb_saque` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tb_transferencia`
--

DROP TABLE IF EXISTS `tb_transferencia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tb_transferencia` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `numero_agencia_destino` varchar(255) NOT NULL,
  `numero_conta_destino` varchar(255) NOT NULL,
  `valor_transferencia` double NOT NULL,
  `conta_id` bigint(20) NOT NULL,
  `numero_agencia_origem` varchar(255) NOT NULL,
  `numero_conta_origem` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK6fyl1osy9mbkxx5kf7knubjcq` (`conta_id`),
  CONSTRAINT `FK6fyl1osy9mbkxx5kf7knubjcq` FOREIGN KEY (`conta_id`) REFERENCES `tb_conta` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tb_transferencia`
--

LOCK TABLES `tb_transferencia` WRITE;
/*!40000 ALTER TABLE `tb_transferencia` DISABLE KEYS */;
INSERT INTO `tb_transferencia` VALUES (1,'0124-4','60694-4',1200,1,'0124-4','6722-4');
/*!40000 ALTER TABLE `tb_transferencia` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'banco'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-03-12 16:24:58
