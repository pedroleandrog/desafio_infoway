**DESAFIO - CRIE UM BANCO DE MANEIRA FÁCIL**

O desafio envolve a implementação de uma API Restful genérica para serviços bancários. Dentre os serviços bancários, a solução implementada, apresenta:

1. Cadastro de Uma Instuição Bancária, isto é, um Banco;
2. Cadastro de uma Agência pertencente a um Banco;
3. Cadastro de Cliente; 
4. Cadastro de Contas juntamente com um Cliente e uma Agencia, ambos já cadastrados; 
5. Extrato Bancário demonstrando a movimentação financeira da Conta; 
6. Operação de Saques; 
7. Operação de Depósitos; 
9. Operação de Transferências; 

---

## Instalação

Os passos a seguir sugerem a importação do projeto na sua IDE preferida

1. Clique em **Clonar** para importar o projeto via git ou faça o download do repositório clicando no botão ao lado de Clonar.
2. Se você fez o download do repositório, faça agora a importação do projeto na sua IDE.

---

## Configuração do Banco de Dados

Antes de executar o projeto pela primeira vez, é necessário fazer a configuração do Banco de Dados. 
Isso é referente a inserção das credenciais do Servidor de Banco de Dados instalado e configurado na sua máquina. 

1. Abra o arquivo **application.properties** em /src/main/resources/.
2. Configure a URL do banco de acordo com o server que você utiliza em sua máquina.
3. Mantenha o **nome** do banco de dados como **banco** mesmo.
3. Informe o **username** logo abaixo.
3. E em seguida a **senha**.
5. A última configuração **update** é uma propriedade de atualização do banco de dados criado toda vez que a aplicação é executada, então deixe como está.

---

## Informações IMPORTANTES

Esteja atento para algumas informações importantes desse projeto:

1. Há um dump do banco de dados que você também pode utilizar para criar as tabelas e ter acesso aos dados que já foram inseridos. Aproveite.
2. Utilize o POSTMAN para acessar os recursos:
3. As credenciais de **Cliente** como client-id e client-secret estão disponíveis em **OAuthConfig.java**
4. As credenciais de **Usuário** como client-id e client-secret estão disponíveis em **applications.properties** em /src/main/resources/